import random

# define function that ensures input is an interger not string
def guessNum(message):
    while True:
        try:
            my_guess = int(input(message))
        except ValueError:
            print("Not an integer! Try again.")
            continue
        else:
            return my_guess

# generate a random interger into variable
random_number = random.randint(1,2020)

# Enter a number
num = guessNum("Enter a number: ")

# check if your input is same as randomly generated number
if num == random_number:
    print("Good Guess!! the number is ", random_number)
elif num > random_number:
    print("Wrong Answer!! The number", num, "is larger than", random_number)
else:
    print("Wrong Answer!! The number", num, "is smaller than", random_number)
